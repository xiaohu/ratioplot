import ROOT as R
from ratioplotlib.mkcanvas import *
from ratioplotlib.util import *

### inputs ###
# +
#outpdf = 'HH_bbll_3channel.pdf'
#legend_title  =  '#kappa_{#lambda} = 1'
#atlas_label = 'Simulation Internal' # 'Generator-level' # 'Simulation Internal' 
#list_title    = [ 'bbWW', 'bbZZ', 'bbtautau' ]
#list_filenm   = [ '../../MCtruth/hh_mc_truth0/job-bbll-bbWW/hist-mkshower_output.root',
#                  '../../MCtruth/hh_mc_truth0/job-bbll-bbZZ/hist-mkshower_output.root',
#                  '../../MCtruth/hh_mc_truth0/job-bbll-bbtt/hist-mkshower_output.root', ]
#list_style    = [ [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
#                  [ 1, 2, R.kRed, 0 ],
#                  [ 1, 2, R.kBlack, 0 ], ]
#list_draw_opt = [ 'hist',
#                  'hist',
#                  'hist', ]
#list_norm     = [ 1.0, 1.0, 1.0 ] # NOT norm them, as they are before/after selections

#outpdf = 'HH_bbll_bbWW_011020.pdf'
#legend_title  =  'bbWW'
#atlas_label = 'Simulation Internal' # 'Generator-level' # 'Simulation Internal' 
#list_title    = [ '#kappa_{#lambda} = 1', '#kappa_{#lambda} = 0', '#kappa_{#lambda} = 10' , '#kappa_{#lambda} = 20']
#list_filenm   = [ '../../MCtruth/hh_mc_truth0/job-bbll-bbWW_cHHHp01d0/hist-mkshower_output.root',
#                  '../../MCtruth/hh_mc_truth0/job-bbll-bbWW_cHHHp00d0/hist-mkshower_output.root',
#                  '../../MCtruth/hh_mc_truth0/job-bbll-bbWW_cHHHp10d0/hist-mkshower_output.root',
#                  '../../MCtruth/hh_mc_truth0/job-bbll-bbWW_cHHHp20d0/hist-mkshower_output.root', ]
#list_style    = [ [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
#                  [ 1, 2, R.kRed, 0 ],
#                  [ 1, 2, R.kBlack, 0 ],
#                  [ 1, 2, R.kGreen, 0 ], ]
#list_draw_opt = [ 'hist',
#                  'hist',
#                  'hist',
#                  'hist', ]
#list_norm     = [ 1.0, 1.0, 1.0, 1.0 ] # NOT norm them, as they are before/after selections

#outpdf = 'HH_bbll_reweightHHm_cHHHp00d0.pdf'
#legend_title  =  'bbWW #kappa_{#lambda} = 0'
#atlas_label = 'Simulation Internal' # 'Generator-level' # 'Simulation Internal' 
#list_title    = [ 'MC generated', 'MC reweighted from #kappa_{#lambda} = 1' ]
#list_filenm   = [ '../../MCtruth/hh_mc_truth0/job-bbll-bbWW_cHHHp00d0/hist-mkshower_output.root',
#                  '../../MCtruth/hh_mc_truth0/job-bbll-bbWW_cHHHp00d0_fromcHHHp01d0/hist-mkshower_output.root', ]
#list_style    = [ [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
#                  [ 1, 2, R.kRed, 0 ], ]
#list_draw_opt = [ 'hist',
#                  'hist', ]
#list_norm     = [ 1.0, 1.0 ] # NOT norm them, as they are before/after selections

#outpdf = 'HH_bbll_reweightHHm_cHHHp20d0.pdf'
#legend_title  =  'bbWW #kappa_{#lambda} = 20'
#atlas_label = 'Simulation Internal' # 'Generator-level' # 'Simulation Internal' 
#list_title    = [ 'MC generated', 'MC reweighted from #kappa_{#lambda} = 10' ]
#list_filenm   = [ '../../MCtruth/hh_mc_truth0/job-bbll-bbWW_cHHHp20d0/hist-mkshower_output.root',
#                  '../../MCtruth/hh_mc_truth0/job-bbll-bbWW_cHHHp20d0_fromcHHHp10d0/hist-mkshower_output.root', ]
#list_style    = [ [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
#                  [ 1, 2, R.kRed, 0 ], ]
#list_draw_opt = [ 'hist',
#                  'hist', ]
#list_norm     = [ 1.0, 1.0 ] # NOT norm them, as they are before/after selections

outpdf = 'HH_bbll_bbWW_rel21_6_17_newworkflow.pdf'
legend_title  =  'bbWW'
atlas_label = 'Simulation Internal' # 'Generator-level' # 'Simulation Internal' 
list_title    = [ '#kappa_{#lambda} = 1', '#kappa_{#lambda} = 10' ]
list_filenm   = [ '../../MCtruth/hh_mc_truth0/job-bbll-bbWW_cHHHp01d0_21.6.17/hist-test_bbWW_cHHH01d0.root',
                  '../../MCtruth/hh_mc_truth0/job-bbll-bbWW_cHHHp10d0_21.6.17/hist-test_bbWW_cHHH10d0.root', ]
list_style    = [ [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
                  [ 1, 2, R.kGreen, 0 ], ]
list_draw_opt = [ 'hist',
                  'hist', ]
list_norm     = [ 1.0, 1.0, ] # NOT norm them, as they are before/after selections

### inputs ###

# get all histogram names
list_histogram_names = listhnm( list_filenm )
print(list_histogram_names)

# ATLAS style
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasStyle.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasLabels.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasUtils.C")
R.SetAtlasStyle()

for i,hnm in enumerate(list_histogram_names):
  print("Processing histogram {0}".format(hnm))

  # get list of histograms
  list_h, __list_f = listh( hnm, list_filenm )
  print(list_h)
  # normalisat to 1
  try:
    list_norm
  except NameError:
    print("list_norm is not define. Do NOT norm any hist!")
  else:
    print("list_norm is defined. Norm hist.")
    norm2unity(list_h,list_norm)
  # set styles
  hstyle(list_h, list_style)
  # rebin if needed
  if 'H1_pT' in hnm or 'H1_E' in hnm : hrebin(list_h, 1, [0,500])
  if 'H2_pT' in hnm or 'H2_E' in hnm : hrebin(list_h, 1, [0,500])
  if '_eta' in hnm or '_phi' in hnm : pass #hrebin( list_h, 4)
  if 'HH_pT' in hnm : hrebin(list_h, 20, [0,500])
  if 'HH_E' in hnm : pass #hrebin(list_h, 40)
  if 'HH_m' in hnm : hrebin(list_h, 40, [200,800])
  if 'HH_dEta' in hnm  or 'HH_dPhi' in hnm or 'HH_dR' in hnm : pass #hrebin(list_h, 2)
  if 'gq_pT' in hnm : pass #hrebin(list_h, 10)
  if 'gq_E' in hnm : pass #hrebin(list_h, 20)
  if 'bbbb_pT' in hnm :
    list_h = hrebin(list_h, range(0,100,10)+range(100,300,20), [0,250])
  if 'bbbb_m' in hnm : hrebin(list_h, 20)
  if 'b1_pT' in hnm or 'b2_pT' in hnm: hrebin(list_h, 1, [0,500])
  if 'el1_pT' in hnm or 'el2_pT' in hnm :  hrebin(list_h, 7, [0,350])
  if 'mu1_pT' in hnm or 'mu2_pT' in hnm :  hrebin(list_h, 7, [0,350])
  if 'tau1_pT' in hnm or 'tau2_pT' in hnm :  hrebin(list_h, 7, [0,350])
  if 'lep1_pT' in hnm or 'lep1_viaemu_pT' in hnm or 'lep1_viatau_pT' in hnm :  hrebin(list_h, 7, [0,350])
  if 'lep2_pT' in hnm or 'lep2_viaemu_pT' in hnm or 'lep2_viatau_pT' in hnm :  hrebin(list_h, 7, [0,350])
  if ('el' in hnm or 'mu' in hnm or 'tau' in hnm or 'lep' in hnm ) and '_eta' in hnm: hrebin(list_h, 5)
  if 'weight' in hnm :
    hrange(list_h, -0.5, 0.5 )
    for _i,_h in enumerate(list_h):
      print( 'Weight pos:neg [{0}] {1} {2} '.format( list_title[_i], _h.Integral(1,_h.FindBin(0)-1), _h.Integral(_h.FindBin(0),_h.GetNbinsX()) ) )

  islogy = False
  #islogy = True if 'HH_pT' in hnm else False # compare Powheg MG5_aMC@NLO only

  # make canvas with ratio
  if 'tau' in hnm:
    c, _aux_c = mkcanvas( [list_h,list_title], legend_title=legend_title, legendx=0.55, atlas_label=atlas_label, islogy=islogy, draw_opt=list_draw_opt, ratio_range=[0,0.15])
  else:
    c, _aux_c = mkcanvas( [list_h,list_title], legend_title=legend_title, legendx=0.55, atlas_label=atlas_label, islogy=islogy, draw_opt=list_draw_opt, ratio_range=[0.4,1.4])
  # write to pdf
  if i == 0: c.Print( outpdf+'(' )
  if i > 0 and i < len(list_histogram_names) - 1: c.Print( outpdf )
  if i == len(list_histogram_names) - 1: c.Print( outpdf+')' )

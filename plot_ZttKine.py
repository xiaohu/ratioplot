import ROOT as R
from ratioplotlib.mkcanvas import *
from ratioplotlib.util import *
import sys

### inputs ###
# +
#ischannel = 'isZtautau_emu' # isZtautau_emu isZtautau_etau_1prong isZtautau_etau_3prong isZtautau_mutau_1prong isZtautau_mutau_3prong
#outpdf = 'Ztt_Kine_{0}.pdf'.format(ischannel)
#atlas_label = 'Internal' # 'Generator-level'
#list_title    = [ 'PowhegPythia8',
#                  'Sherpa221' ]
#list_filenm   = [ '../anatree/PowhegPythia8_Ztt_hist_{0}.root'.format(ischannel),
#                  '../anatree/Sherpa221_Ztt_hist_{0}.root'.format(ischannel) ]
# +
#ischannel = 'isZtautau_mutau_3prong' # isZtautau_emu isZtautau_etau_1prong isZtautau_etau_3prong isZtautau_mutau_1prong isZtautau_mutau_3prong
#outpdf = 'Ztt_Kine_{0}_mZ60.pdf'.format(ischannel)
#atlas_label = 'Internal' # 'Generator-level'
#list_title    = [ 'PowhegPythia8',
#                  'Sherpa221' ]
#list_filenm   = [ '../anatree/cut_z_mass_60/PowhegPythia8_Ztt_hist_{0}.root'.format(ischannel),
#                  '../anatree/cut_z_mass_60/Sherpa221_Ztt_hist_{0}.root'.format(ischannel) ]
# +
#ischannel = 'isZtautau_mutau_3prong' # isZtautau_emu isZtautau_etau_1prong isZtautau_etau_3prong isZtautau_mutau_1prong isZtautau_mutau_3prong
#outpdf = 'Ztt_Kine_{0}_mZ60recoall.pdf'.format(ischannel)
#atlas_label = 'Internal' # 'Generator-level'
#list_title    = [ 'PowhegPythia8',
#                  'Sherpa221' ]
#list_filenm   = [ '../anatree/cut_z_mass_60_recoall/PowhegPythia8_Ztt_hist_{0}.root'.format(ischannel),
#                  '../anatree/cut_z_mass_60_recoall/Sherpa221_Ztt_hist_{0}.root'.format(ischannel) ]
## +
####
#list_style    = [ [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
#                  [ 1, 2, R.kRed, 0 ] ]
#list_draw_opt = [ 'hist',
#                  'hist' ]
#legend_title  =  ischannel
# +
# pass from command lines
ischannel = sys.argv[1] # isZtautau_emu isZtautau_etau_1prong isZtautau_etau_3prong isZtautau_mutau_1prong isZtautau_mutau_3prong
outpdf = sys.argv[2]
atlas_label = 'Internal' # 'Generator-level'
list_title    = [ 'PowhegPythia8',
                  'Sherpa221' ]
list_filenm   = [ sys.argv[3],
                  sys.argv[4], ]
# +
###
list_style    = [ [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
                  [ 1, 2, R.kRed, 0 ] ]
list_draw_opt = [ 'hist',
                  'hist' ]
legend_title  =  ischannel
### inputs ###

# get all histogram names
list_histogram_names = listhnm( list_filenm )
print(list_histogram_names)

# ATLAS style
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasStyle.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasLabels.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasUtils.C")
R.SetAtlasStyle()

for i,hnm in enumerate(list_histogram_names):
  print("Processing histogram {0}".format(hnm))


  # get list of histograms
  list_h, __list_f = listh( hnm, list_filenm )
  print(list_h)
  # normalisat to 1
  norm2unity(list_h)
  # set styles
  hstyle(list_h, list_style)

  islogy = True
  #islogy = True if 'HH_pT' in hnm else False # compare Powheg MG5_aMC@NLO only

  # make canvas with ratio
  c, _aux_c = mkcanvas( [list_h,list_title], legend_title=legend_title, legendx=0.55, atlas_label=atlas_label, islogy=islogy, draw_opt=list_draw_opt)
  # write to pdf
  if i == 0: c.Print( outpdf+'(' )
  if i > 0 and i < len(list_histogram_names) - 1: c.Print( outpdf )
  if i == len(list_histogram_names) - 1: c.Print( outpdf+')' )

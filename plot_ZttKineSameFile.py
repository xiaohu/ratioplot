import ROOT as R
from ratioplotlib.mkcanvas import *
from ratioplotlib.util import *

### inputs ###
# plot distributions for all subchannels in one file
# +
outpdf = 'Ztt_Kine_allchannel.pdf'
atlas_label = 'Internal' # 'Generator-level'
list_title    = [ 'Ztt etau 1prong',
                  'Ztt etau 3prong',
                  'Ztt mutau 1prong',
                  'Ztt mutau 3prong',
                  'Ztt emu',]
list_filenm   = [ '../anatree/cut_z_mass_60/Sherpa221_Ztt_hist_isZtautau_etau_1prong.root',
                  '../anatree/cut_z_mass_60/Sherpa221_Ztt_hist_isZtautau_etau_3prong.root',
                  '../anatree/cut_z_mass_60/Sherpa221_Ztt_hist_isZtautau_mutau_1prong.root',
                  '../anatree/cut_z_mass_60/Sherpa221_Ztt_hist_isZtautau_mutau_3prong.root',
                  '../anatree/cut_z_mass_60/Sherpa221_Ztt_hist_isZtautau_emu.root',]
list_style    = [ [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
                  [ 2, 2, R.kRed, 0 ],
                  [ 1, 2, R.kBlue, 0 ],
                  [ 2, 2, R.kRed, 0 ],
                  [ 1, 2, R.kBlack, 0 ],
                ]
list_draw_opt = [ 'hist',
                  'hist',
                  'hist',
                  'hist',
                  'hist',]
legend_title  =  'Sherpa211'
### inputs ###

# get all histogram names
#list_histogram_names = listhnm( list_filenm )
list_histogram_names = [
  'h_z_pT',
]
 
print(list_histogram_names)

# ATLAS style
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasStyle.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasLabels.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasUtils.C")
R.SetAtlasStyle()

for i,hnm in enumerate(list_histogram_names):
  print("Processing histogram {0}".format(hnm))


  # get list of histograms
  list_h, __list_f = listh( hnm, list_filenm )
  print(list_h)
  # normalisat to 1
  norm2bin1(list_h,'binomial')
  # set styles
  hstyle(list_h, list_style)

  islogy = True
  #islogy = True if 'HH_pT' in hnm else False # compare Powheg MG5_aMC@NLO only

  # make canvas with ratio
  c, _aux_c = mkcanvas( [list_h,list_title], ratiorange=[0.85,1.15], legend_title=legend_title, legendx=0.55, atlas_label=atlas_label, islogy=islogy, draw_opt=list_draw_opt)
  # write to pdf
  if i == 0: c.Print( outpdf+'(' )
  if i > 0 and i < len(list_histogram_names) - 1: c.Print( outpdf )
  if i == len(list_histogram_names) - 1: c.Print( outpdf+')' )

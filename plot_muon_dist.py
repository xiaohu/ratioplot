import ROOT as R
from ratioplotlib.mkcanvas import *
from ratioplotlib.util import *

### inputs ###
# +
year=2018
outpdf = 'Muon_dist_{0}.pdf'.format(year)
legend_title  =  'Year {0}'.format(year)
status_label = 'Preliminary'
list_title    = [ 'Data',
                  'DY aMC@NLO FXFX' ]
list_filenm   = [ '../muon-fastfeedbacktool/job/merged-singlemuon_{0}.root'.format(year),
                  '../muon-fastfeedbacktool/job/merged-DYJetsToLL_M-50-{0}.root'.format(year)]
list_style    = [ [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
                  [ 1, 2, R.kRed, 0 ] ]
list_draw_opt = [ 'hist',
                  'hist' ]
### inputs ###

# get all histogram names
list_histogram_names = listhnm( list_filenm )
print(list_histogram_names)

# ATLAS style
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasStyle.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasLabels.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasUtils.C")
R.SetAtlasStyle()

for i,hnm in enumerate(list_histogram_names):
  print("Processing histogram {0}".format(hnm))

  # get list of histograms
  list_h, __list_f = listh( hnm, list_filenm )
  print(list_h)
  # normalisat to 1
  norm2unity(list_h)
  # set styles
  hstyle(list_h, list_style)
  # rebin if needed
  #
  # extract bin info
  # 'binmode_{0}__id_{1}__{2}_{3:.1f}_{4:.1f}'
  _l = hnm.split('__')
  print(_l)
  _binmode = 'Mode {0}'.format(_l[0].split('_')[1])
  _id = '_'.join(_l[1].split('_')[1:])
  _varl = _l[2].split('_')
  _var = '_'.join(_varl[:-4])
  _from = _varl[-4]
  _to = _varl[-3]
  _thisbin = ''
  print(_var)
  if _var == 'Muon_pt':
    _thisbin = '{0} < {2} < {1}'.format(_from,_to, 'p_{T}')
  if _var == 'Muon_eta':
    _thisbin = '{0} < |#eta| < {1}'.format(_from,_to)
  print(_thisbin)
  _x = 0.22
  _y = 0.55
  _morelabel = [ [_x,_y,_id], [_x,_y-0.05,_binmode], [_x,_y-0.10,_thisbin] ]

  # make canvas with ratio
  c, _aux_c = mkcanvas( [list_h,list_title], legend_title=legend_title, legendx=_x, status_label=status_label, draw_opt=list_draw_opt, morelabel=_morelabel)
  # write to pdf
  if i == 0: c.Print( outpdf+'(' )
  if i > 0 and i < len(list_histogram_names) - 1: c.Print( outpdf )
  if i == len(list_histogram_names) - 1: c.Print( outpdf+')' )

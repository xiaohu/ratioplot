import ROOT as R
from ratioplotlib.mkcanvas import *
from ratioplotlib.util import *
import sys

# this compares linearly combined shapes between basis 0,1,20 and 0,1,5

### inputs ###
#lookatlambda = sys.argv[1]
#channel = 'bbWW' # bbWW bbZZ bbtt
#
#scheme = ['0,1,20', '0,1,5'] # actual value [basis lambda] increasing order of lambda !!!
#outpdf = 'HH_LCbasisstudy-{1}-PowhegFT-lambda{0}.pdf'.format(lookatlambda, channel)
#
#scheme1_str=['00','01','20'] # string in file names [basis lambda] increasing order of lambda !!!
#scheme2_str=['00','01','05'] # string in file names [basis lambda] increasing order of lambda !!!
#
#legend_title  =  '#kappa_{#lambda} LC basis studies'
#atlas_label = 'Simulation Internal' # 'Generator-level' # 'Simulation Internal' 
#list_title    = [ 'Basis #kappa_{{#lambda}} = {0}'.format(scheme[0]),
#                  'Basis #kappa_{{#lambda}} = {0}'.format(scheme[1]), ]
## only the basis samples here ...
#list_filenm   = [ ['../../MCtruth/hh_mc_truth0/job-bbll-{0}_cHHHp{1}d0/hist-mkshower_output.root'.format(channel, _kl) for _kl in scheme1_str],
#                  ['../../MCtruth/hh_mc_truth0/job-bbll-{0}_cHHHp{1}d0/hist-mkshower_output.root'.format(channel, _kl) for _kl in scheme2_str],] # [ target, [basis1,2,3] ]
#list_style    = [ [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
#                  [ 1, 2, R.kRed, 0 ],
#                ]
#list_draw_opt = [ 'histe',
#                  'histe' ]


lookatlambda = sys.argv[1]
channel = 'bbWW' # bbWW bbZZ bbtt

scheme = ['0,1,20', '0,1,10'] # actual value [basis lambda] increasing order of lambda !!!
outpdf = 'HH_LCbasisstudy-{1}-PowhegFT-lambda{0}.pdf'.format(lookatlambda, channel)

scheme1_str=['00','01','20'] # string in file names [basis lambda] increasing order of lambda !!!
scheme2_str=['00','01','10'] # string in file names [basis lambda] increasing order of lambda !!!

legend_title  =  '#kappa_{#lambda} LC basis studies'
atlas_label = 'Simulation Internal' # 'Generator-level' # 'Simulation Internal' 
list_title    = [ 'Basis #kappa_{{#lambda}} = {0}'.format(scheme[0]),
                  'Basis #kappa_{{#lambda}} = {0}'.format(scheme[1]), ]
# only the basis samples here ...
list_filenm   = [ ['../../MCtruth/hh_mc_truth0/job-bbll-{0}_cHHHp{1}d0/hist-mkshower_output.root'.format(channel, _kl) for _kl in scheme1_str],
                  ['../../MCtruth/hh_mc_truth0/job-bbll-{0}_cHHHp{1}d0/hist-mkshower_output.root'.format(channel, _kl) for _kl in scheme2_str],] # [ target, [basis1,2,3] ]
list_style    = [ [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
                  [ 1, 2, R.kRed, 0 ],
                ]
list_draw_opt = [ 'histe',
                  'histe' ]
### inputs ###

# get all histogram names
list_histogram_names = listhnm( list_filenm[0] )
print(list_histogram_names)

# ATLAS style
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasStyle.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasLabels.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasUtils.C")
R.SetAtlasStyle()

for i,hnm in enumerate(list_histogram_names):
  print("Processing histogram {0}".format(hnm))

  list_h = []
  for _i, _list_file_perscheme in enumerate(list_filenm):
    list_h_base, __list_f_base = listh( hnm, _list_file_perscheme )
    h_lc = lc_lambda( scheme[_i], float(lookatlambda), 1, list_h_base )
    list_h.append(h_lc)
  #list_h_base, __list_f_base = listh( hnm, list_filenm[0] )
  #h_lc_0 = lc_lambda( scheme[0], float(lookatlambda), 1, list_h_base )
  #list_h.append(h_lc_0)
  #list_h_base, __list_f_base = listh( hnm, list_filenm[1] )
  #h_lc_1 = lc_lambda( scheme[1], float(lookatlambda), 1, list_h_base )
  #list_h.append(h_lc_1)
  print(list_h)

  # normalisat to 1
  norm2unity(list_h)

  # set styles
  hstyle(list_h, list_style)
  # rebin if needed
  if 'H1_pT' in hnm or 'H1_E' in hnm : hrebin(list_h, 1, [0,500])
  if 'H2_pT' in hnm or 'H2_E' in hnm : hrebin(list_h, 1, [0,500])
  if '_eta' in hnm or '_phi' in hnm : pass #hrebin( list_h, 4)
  if 'HH_pT' in hnm : hrebin(list_h, 20, [0,500])
  if 'HH_E' in hnm : pass #hrebin(list_h, 40)
  if 'HH_m' in hnm :
    hrebin(list_h, 40, [200,800])
    for _hh_m in list_h:
      for _hh_ibin in range(1,_hh_m.GetNbinsX()+1 ):
        _hh_c = _hh_m.GetBinContent( _hh_ibin )
        if _hh_c < 0:
          print('DDDDDDDDDD HIST {0} BIN {1} CONTENT {2} NEGATIVE!!!'.format(_hh_m.GetName(), _hh_ibin, _hh_c ))
  if 'HH_dEta' in hnm  or 'HH_dPhi' in hnm or 'HH_dR' in hnm : pass #hrebin(list_h, 2)
  if 'gq_pT' in hnm : pass #hrebin(list_h, 10)
  if 'gq_E' in hnm : pass #hrebin(list_h, 20)
  if 'bbbb_pT' in hnm :
    list_h = hrebin(list_h, range(0,100,10)+range(100,300,20), [0,250])
  if 'bbbb_m' in hnm : hrebin(list_h, 20)
  if 'b1_pT' in hnm or 'b2_pT' in hnm: hrebin(list_h, 1, [0,500])
  if 'el1_pT' in hnm or 'el2_pT' in hnm :  hrebin(list_h, 7, [0,350])
  if 'mu1_pT' in hnm or 'mu2_pT' in hnm :  hrebin(list_h, 7, [0,350])
  if 'tau1_pT' in hnm or 'tau2_pT' in hnm :  hrebin(list_h, 7, [0,350])
  if 'lep1_pT' in hnm or 'lep1_viaemu_pT' in hnm or 'lep1_viatau_pT' in hnm :  hrebin(list_h, 7, [0,350])
  if 'lep2_pT' in hnm or 'lep2_viaemu_pT' in hnm or 'lep2_viatau_pT' in hnm :  hrebin(list_h, 7, [0,350])
  if ('el' in hnm or 'mu' in hnm or 'tau' in hnm or 'lep' in hnm ) and '_eta' in hnm: hrebin(list_h, 5)
  if 'weight' in hnm :
    hrange(list_h, -0.5, 0.5 )
    for _i,_h in enumerate(list_h):
      print( 'Weight pos:neg [{0}] {1} {2} '.format( list_title[_i], _h.Integral(1,_h.FindBin(0)-1), _h.Integral(_h.FindBin(0),_h.GetNbinsX()) ) )
  islogy = True if 'HH_pT' in hnm else False # compare Powheg MG5_aMC@NLO only
  #islogy = True if 'HH_m' in hnm else False # compare Powheg MG5_aMC@NLO only

  # make canvas with ratio
  c, _aux_c = mkcanvas( [list_h,list_title], legend_title=legend_title, legendx=0.55, islogy=islogy, atlas_label=atlas_label, draw_opt=list_draw_opt, ratio_range=[0,2], cmperror=True)
  # write to pdf
  if i == 0: c.Print( outpdf+'(' )
  if i > 0 and i < len(list_histogram_names) - 1: c.Print( outpdf )
  if i == len(list_histogram_names) - 1: c.Print( outpdf+')' )

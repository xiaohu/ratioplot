import ROOT as R
from util import *

# inputs: list_h_nm [list_h, list_nm], list_h[0] is the denominator
# they are list of histograms and list of their legend entry names
# outputs: canvas
def mkcanvas( list_h_nm, bottompane=0.3, xtitle=None, ytitle=None, yrange=None, ratiorange=[0.5,1.5], legend_title=None, legendx=None, legendy=None, islogy=False, islogx=False,  status_label= 'Preliminary', draw_opt=[], cmperror=False, morelabel=[], autolegend=True ):

  R.gROOT.SetBatch(1)

  list_h = list_h_nm[0]

  # ATLAS style
  # call it as top level of the scripts, otherwise the style is cleaned up in python

  # setup canvas and pads
  c = R.TCanvas('c','c',800,800)
  padup = R.TPad("padup", "padup", 0, bottompane, 1, 1.0)
  padup.SetBottomMargin(0.02)
  padup.Draw()
  c.cd()
  paddn = R.TPad("paddn", "paddn", 0, 0, 1, bottompane)
  paddn.SetTopMargin(0.02)
  paddn.SetBottomMargin(0.3)
  paddn.Draw()

  # draw histograms in upper pad
  padup.cd()
  ymax, _maxh = findmax( list_h )
  ymin, _minh = findmin( list_h )
  ymin_abs, _minh_abs = findmin_abs( list_h )
  for i,h in enumerate(list_h):
    if i == 0:
      if yrange is not None:
        h.SetMaximum( yrange[1] )
        h.SetMinimum( yrange[0] )
      else:
        h.SetMaximum( ymax * 1.2 ) # max in y 20%
        h.SetMinimum( 0 )
      if ytitle is not None:
        h.SetYTitle( ytitle )
      else:
        h.SetYTitle('A.U.')
      h.GetXaxis().SetLabelSize(0)
      if islogy:
        h.SetMaximum( ymax*1000 ) # max in y 1000x
        #h.SetMinimum( ymin_abs/10 ) # findmin_abs returns 0 if min=0 which is not good for logy
        h.SetMinimum( ymax*0.0001 ) # 4 orders of mag smaller
      if len(draw_opt) == 0:
        h.Draw()
      else:
        h.Draw( draw_opt[i] )
    else:
      if len(draw_opt) == 0:
        h.Draw('same')
      else:
        h.Draw( 'same'+draw_opt[i] )

  if islogy:
    padup.SetLogy()
  if islogx:
    padup.SetLogx()

  # mk legend in upper pad
  _leg = mklegend( list_h_nm, legend_title, legendx, legendy, status_label, autoadjust=autolegend )

  # more labels
  if len(morelabel) > 0:
    for _thelabel in morelabel:
      R.myText(_thelabel[0],_thelabel[1],1,_thelabel[2])

  # draw ratios in down pad
  paddn.cd()
  # calculte ratio and error
  list_ratio = []
  for i,h in enumerate(list_h):
    newnm = h.GetName()+'_ratio'
    newh = h.Clone(newnm)
    list_ratio.append(newh)

    # denominator 1 +/- its own error%
    if i == 0:
      for ibin in range(newh.GetNbinsX()):
        ibin = ibin + 1
        newh.SetBinError( ibin, h.GetBinError(ibin) / h.GetBinContent(ibin) if h.GetBinContent(ibin)!=0 else 0 )
        newh.SetBinContent( ibin, 1 )
      newh.SetMaximum(ratiorange[1]) # 1.5 ratio y range, 3.2 for kl=2.5 LC method
      newh.SetMinimum(ratiorange[0]) # 0.5 ratio y range, 0 for kl=2.5 LC method

      newh.SetYTitle('Ratios')
      if xtitle is not None:
        newh.SetXTitle( xtitle )
      if cmperror:
        newh.SetYTitle('Compare error only')
      if newh.GetXaxis().GetTitle() == '': # if no x title, use hist title
        newh.GetXaxis().SetTitle( newh.GetTitle() )

      newh.GetXaxis().SetTitleSize( newh.GetXaxis().GetTitleSize()*2 )
      newh.GetYaxis().SetTitleSize( newh.GetYaxis().GetTitleSize()*2 )
      newh.GetXaxis().SetTitleOffset( newh.GetXaxis().GetTitleOffset()*0.75 )
      newh.GetYaxis().SetTitleOffset( newh.GetYaxis().GetTitleOffset()*0.5 )
      newh.SetLabelSize( R.gStyle.GetLabelSize()*2 )
      #newh.GetYaxis().SetNdivisions(5)
      newh.GetYaxis().SetNdivisions(505)
      newh.GetYaxis().SetLabelSize( newh.GetYaxis().GetLabelSize()*2.5 )
      newg = R.TGraphErrors(newh)
      if not cmperror:
        newh.Draw('E3 hist')
        newg.SetFillColor(R.kBlack)
        newg.SetFillStyle(3554)
        newg.Draw('3')
      else:
        newh.SetLineWidth(5)
        newh.Draw('histe1')
        
      list_ratio.append(newg)
      #if len( draw_opt ) == 0:
      #  newh.Draw( 'E2hist' )
      #else:
      #  newh.Draw( 'E1hist'+draw_opt[i] )

    # numerators ratio +/-  its own error%
    else:
      newh.Divide( list_h[0] )
      for ibin in range(newh.GetNbinsX()):
        ibin = ibin + 1
        if cmperror:
          newh.SetLineWidth(5)
          newh.SetBinContent( ibin, 1 )
        newh.SetBinError( ibin, h.GetBinError(ibin) / h.GetBinContent(ibin) if h.GetBinContent(ibin)!=0 else 0 )
      newh.Draw('same E1 hist')
      #if len( draw_opt ) == 0:
      #  newh.Draw( 'same E1 hist' )
      #else:
      #  newh.Draw( 'same E1 hist'+draw_opt[i] )

  if islogx:
    paddn.SetLogx()

  c.cd()
  c.Update()
  return c, [list_ratio, _leg]


# inputs: list_h_nm [list_h, list_nm], list_h[0] is B, list_h[1] is S !!! only TWO !!!
# ratio pane show relative S/sqrt(B) for cutting >= ibin (isabove) or <= ibin (false)
# outputs: canvas
def mkcanvas_significance( list_h_nm, isabove=True, legend_title=None, legendx=None, islogy=False, status_label= 'Preliminary', draw_opt=[] ):

  R.gROOT.SetBatch(1)

  # atm support s,b two histograms
  if len(list_h_nm) != 2:
    print("mkcanvas_significance() requires hist list size=2")
    exit()

  list_h = list_h_nm[0]

  # ATLAS style
  # call it as top level of the scripts, otherwise the style is cleaned up in python

  # setup canvas and pads
  c = R.TCanvas('c','c',800,800)
  padup = R.TPad("padup", "padup", 0, 0.3, 1, 1.0)
  padup.SetBottomMargin(0.02)
  padup.Draw()
  c.cd()
  paddn = R.TPad("paddn", "paddn", 0, 0, 1, 0.3)
  paddn.SetTopMargin(0.02)
  paddn.SetBottomMargin(0.3)
  paddn.Draw()

  # draw histograms in upper pad
  padup.cd()
  ymax, _maxh = findmax( list_h )
  ymin, _minh = findmin( list_h )
  ymin_abs, _minh_abs = findmin_abs( list_h )
  for i,h in enumerate(list_h):
    if i == 0:
      h.SetMaximum( ymax * 1.2 ) # max in y 20%
      h.SetMinimum( 0 )
      h.SetYTitle('A.U.')
      h.GetXaxis().SetLabelSize(0)
      if islogy:
        h.SetMaximum( ymax*1000 ) # max in y 1000x
        #h.SetMinimum( ymin_abs/10 ) # findmin_abs returns 0 if min=0 which is not good for logy
        h.SetMinimum( ymax*0.0001 ) # 4 orders of mag smaller
      if len(draw_opt) == 0:
        h.Draw()
      else:
        h.Draw( draw_opt[i] )
    else:
      if len(draw_opt) == 0:
        h.Draw('same')
      else:
        h.Draw( 'same'+draw_opt[i] )

  if islogy:
    padup.SetLogy()

  # mk legend in upper pad
  _leg = mklegend( list_h_nm, legend_title, legendx, status_label )

  # draw ratios in down pad
  paddn.cd()
  # calculte ratio and error
  list_ratio = []

  # [0] is B -> sqrt(B)
  hist_sqrtb = hintegral( isabove, list_h[0] )
  #hsqrt(hist_sqrtb)
  list_ratio.append(hist_sqrtb)
  # [1] is S -> s/sqrt(B)
  hist_sig = hintegral( isabove, list_h[1] )
  hist_sig.Divide(hist_sqrtb)
  list_ratio.append(hist_sig)
  # draw
  #if isabove:
  #  hist_sig.SetYTitle('Rel. S#/#sqrt{B} above cut')
  #else:
  #  hist_sig.SetYTitle('Rel. S#/#sqrt{B} below cut')
  if isabove:
    hist_sig.SetYTitle('Rel. S#/B above cut')
  else:
    hist_sig.SetYTitle('Rel. S#/B below cut')
  hist_sig.Draw('hist E1')
  hist_sig.GetXaxis().SetTitleSize( hist_sig.GetXaxis().GetTitleSize()*2 )
  hist_sig.GetYaxis().SetTitleSize( hist_sig.GetYaxis().GetTitleSize()*2 )
  hist_sig.GetXaxis().SetTitleOffset( hist_sig.GetXaxis().GetTitleOffset()*0.75 )
  hist_sig.GetYaxis().SetTitleOffset( hist_sig.GetYaxis().GetTitleOffset()*0.5 )
  hist_sig.SetLabelSize( R.gStyle.GetLabelSize()*2 )
  hist_sig.GetYaxis().SetNdivisions(5)
  hist_sig.GetYaxis().SetLabelSize( hist_sig.GetYaxis().GetLabelSize()*2.5 )

  c.cd()
  c.Update()
  return c, [list_ratio, _leg]


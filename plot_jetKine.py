import ROOT as R
from ratioplotlib.mkcanvas import *
from ratioplotlib.util import *

### inputs ###
# +
mc='mc16a'
outpdf = 'VjetKine_{0}.pdf'.format(mc)
legend_title  =  mc
atlas_label = 'Internal' # 'Generator-level'
list_title    = [ 'PowhegPythia8',
                  'Sherpa221' ]
list_filenm   = [ '../vjetfit/result_mc16adeNominal/ZeePhPy8_{0}.root'.format(mc),
                  '../vjetfit/result_mc16adeNominal/ZeeSherpa221_{0}.root'.format(mc) ]
list_style    = [ [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
                  [ 1, 2, R.kRed, 0 ] ]
list_draw_opt = [ 'hist',
                  'hist' ]
### inputs ###

# get all histogram names
list_histogram_names = listhnm( list_filenm )
print(list_histogram_names)

# ATLAS style
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasStyle.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasLabels.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasUtils.C")
R.SetAtlasStyle()

for i,hnm in enumerate(list_histogram_names):
  print("Processing histogram {0}".format(hnm))

  # get list of histograms
  list_h, __list_f = listh( hnm, list_filenm )
  print(list_h)
  # normalisat to 1
  norm2unity(list_h)
  # set styles
  hstyle(list_h, list_style)

  islogy = False
  islogx = False

  if 'jet_n' in hnm:
    pass
  elif 'jet0_pt' in hnm:
    pass
  elif 'jet1_pt' in hnm:
    pass
    islogy = True
  elif 'jet1_eta' in hnm:
    hrebin(list_h, 2)
    islogy = True
  elif 'dphi_j0_Z' in hnm:
    hrebin(list_h, 2, [2.7,3.15])
  else:
    hrebin(list_h, 5)


  # make canvas with ratio
  c, _aux_c = mkcanvas( [list_h,list_title], ratiorange=[0.90,1.03], legend_title=legend_title, legendx=0.67, atlas_label=atlas_label, islogy=islogy, islogx=islogx, draw_opt=list_draw_opt)
  # write to pdf
  if i == 0: c.Print( outpdf+'(' )
  if i > 0 and i < len(list_histogram_names) - 1: c.Print( outpdf )
  if i == len(list_histogram_names) - 1: c.Print( outpdf+')' )

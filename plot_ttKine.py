import ROOT as R
from ratioplotlib.mkcanvas import *
from ratioplotlib.util import *
import sys

### inputs ###
# pass from command lines
ischannel = sys.argv[1] # isWemu isWetauHad_1prong isWetauHad_3prong isWmutauHad_1prong isWmutauHad_3prong RbbWW
outpdf = sys.argv[2]
# +
#atlas_label = 'Internal' # 'Generator-level'
#list_title    = [ 'nominal',
#                  'fsr muR=0.5',
#                  'fsr muR=2.0' ]
#list_filenm   = [ sys.argv[3],
#                  sys.argv[4],
#                  sys.argv[5], ]
#list_style    = [ [ 1, 2, R.kBlack, 0 ],
#                  [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
#                  [ 1, 2, R.kRed, 0 ] ]
#list_draw_opt = [ 'hist',
#                  'hist',
#                  'hist' ]
#legend_title  =  ischannel
# +
atlas_label = 'Internal' # 'Generator-level'
list_title    = [ 'nominal',
                  'fsr muR=0.5',
                  'fsr muR=0.625',
                  'fsr muR=0.755',
                  'fsr muR=0.875',
                  'fsr muR=1.25',
                  'fsr muR=1.5',
                  'fsr muR=1.75',
                  'fsr muR=2.0',
                ]
list_filenm   = [ sys.argv[3],
                  sys.argv[4],
                  sys.argv[5],
                  sys.argv[6],
                  sys.argv[7],
                  sys.argv[8],
                  sys.argv[9],
                  sys.argv[10],
                  sys.argv[11],
                ]
list_style    = [ [ 1, 2, R.kBlack, 0 ],
                  [ 2, 2, R.kBlack, 0 ], # style, width, color, marker size
                  [ 2, 2, R.kBlue-2, 0 ],
                  [ 2, 2, R.kBlue-1, 0 ],
                  [ 2, 2, R.kBlue, 0 ],
                  [ 2, 2, R.kBlue+1, 0 ],
                  [ 2, 2, R.kBlue+2, 0 ],
                  [ 2, 2, R.kBlue+3, 0 ],
                  [ 2, 2, R.kRed, 0 ]
                ]
list_draw_opt = [ 'hist',
                  'hist',
                  'hist',
                  'hist',
                  'hist',
                  'hist',
                  'hist',
                  'hist',
                  'hist' ]
legend_title  =  ischannel
### inputs ###

# get all histogram names
list_histogram_names = listhnm( list_filenm )
print(list_histogram_names)

# ATLAS style
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasStyle.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasLabels.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasUtils.C")
R.SetAtlasStyle()

for i,hnm in enumerate(list_histogram_names):
  print("Processing histogram {0}".format(hnm))


  # get list of histograms
  list_h, __list_f = listh( hnm, list_filenm )
  print(list_h)

  #if 'j3_pT' in hnm or 'b0_pT' in hnm or 'b1_pT' in hnm :
  #  hrebin( list_h, None, [20,199])

  # normalisat to 1
  #norm2unity(list_h)
  # set styles
  hstyle(list_h, list_style)

  islogy = True
  #islogy = True if 'HH_pT' in hnm else False # compare Powheg MG5_aMC@NLO only

  # make canvas with ratio
  c, _aux_c = mkcanvas( [list_h,list_title], ratiorange=[0.9,1.1], legend_title=legend_title, legendx=0.55, atlas_label=atlas_label, islogy=islogy, draw_opt=list_draw_opt)
  # write to pdf
  if i == 0: c.Print( outpdf+'(' )
  if i > 0 and i < len(list_histogram_names) - 1: c.Print( outpdf )
  if i == len(list_histogram_names) - 1: c.Print( outpdf+')' )

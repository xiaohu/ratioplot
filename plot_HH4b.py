import ROOT as R
from ratioplotlib.mkcanvas import *
from ratioplotlib.util import *

### inputs ###
# +
# m05,-5 p00,0 p01,1 p02p5,2.5 p10,10
lambdastr='p01'
lambdavalue='1'
outpdf = 'HH_lambda{0}-PowhegFT-HW7-PY8.pdf'.format(lambdastr)
legend_title  =  '#kappa_{{#lambda}} = {0}'.format(lambdavalue) #None#
atlas_label = 'Generator-level' # 'Simulation Internal' 
#list_title    = [ 'PH FT HW7',
#                  'PH FT PY8' ]
list_title    = [ 'Powheg-Box-V2 FT Herwig7',
                  'Powheg-Box-V2 FT Pythia8' ]
list_filenm   = [ '../../MCtruth/readtruth0/job-hh4b-kl-{0}-hw7/hist-PH_NLOFT_HW7.root'.format(lambdastr),
                  '../../MCtruth/readtruth0/job-hh4b-kl-{0}-py8/hist-PH_NLOFT_PY8.root'.format(lambdastr) ]
list_style    = [ [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
                  [ 1, 2, R.kRed, 0 ] ]
list_draw_opt = [ 'hist',
                  'hist' ]
### inputs ###

# get all histogram names
list_histogram_names = listhnm( list_filenm )
print(list_histogram_names)

# ATLAS style
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasStyle.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasLabels.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasUtils.C")
R.SetAtlasStyle()

for i,hnm in enumerate(list_histogram_names):
  print("Processing histogram {0}".format(hnm))

  # get list of histograms
  list_h, __list_f = listh( hnm, list_filenm )
  print(list_h)
  # normalisat to 1
  norm2unity(list_h)
  # set styles
  hstyle(list_h, list_style)
  # rebin if needed
  if 'H1_pT' in hnm or 'H1_E' in hnm : pass #hrebin(list_h, 20)
  if 'H2_pT' in hnm or 'H2_E' in hnm : pass #hrebin(list_h, 20)
  if '_eta' in hnm or '_phi' in hnm : pass #hrebin( list_h, 4)
  if 'HH_pT' in hnm : hrebin(list_h, 10)
  if 'HH_E' in hnm : pass #hrebin(list_h, 40)
  if 'HH_m' in hnm : hrebin(list_h, 20)
  if 'HH_dEta' in hnm  or 'HH_dPhi' in hnm or 'HH_dR' in hnm : pass #hrebin(list_h, 2)
  if 'gq_pT' in hnm : pass #hrebin(list_h, 10)
  if 'gq_E' in hnm : pass #hrebin(list_h, 20)
  if 'bbbb_pT' in hnm :
    list_h = hrebin(list_h, range(0,100,10)+range(100,300,20), [0,250])
  if 'bbbb_m' in hnm : hrebin(list_h, 20)
  if 'weight' in hnm :
    hrange(list_h, -0.5, 0.5 )
    for _i,_h in enumerate(list_h):
      print( 'Weight pos:neg [{0}] {1} {2} '.format( list_title[_i], _h.Integral(1,_h.FindBin(0)-1), _h.Integral(_h.FindBin(0),_h.GetNbinsX()) ) )

  islogy = False
  #islogy = True if 'HH_pT' in hnm else False # compare Powheg MG5_aMC@NLO only

  # make canvas with ratio
  c, _aux_c = mkcanvas( [list_h,list_title], legend_title=legend_title, legendx=0.55, atlas_label=atlas_label, islogy=islogy, draw_opt=list_draw_opt)
  # write to pdf
  if i == 0: c.Print( outpdf+'(' )
  if i > 0 and i < len(list_histogram_names) - 1: c.Print( outpdf )
  if i == len(list_histogram_names) - 1: c.Print( outpdf+')' )

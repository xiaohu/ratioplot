import ROOT as R
from ratioplotlib.mkcanvas import *
from ratioplotlib.util import *

### inputs ###
# +
outpdf = 'vbswy-qcd.pdf'
legend_title  =  'MC'
atlas_label = 'Internal' # 'Generator-level'
list_title    = [ 'QCD W#gamma jj', # MUST HAVE B at [0] for S/sqrt(B)
                  'VBS W#gamma jj' ]
list_filenm   = [ '../anatree/QCDWgamma_ev_treereader.root', # MUST HAVE B at [0] for S/sqrt(B)
                  '../anatree/VBSWgamma_ev_treereader.root', ]
list_style    = [ [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
                  [ 1, 2, R.kRed, 0 ] ]
list_draw_opt = [ 'hist',
                  'hist' ]
### inputs ###

# get all histogram names
list_histogram_names = listhnm( list_filenm )
print(list_histogram_names)

# ATLAS style
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasStyle.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasLabels.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasUtils.C")
R.SetAtlasStyle()

for i,hnm in enumerate(list_histogram_names):
  print("Processing histogram {0}".format(hnm))

  # get list of histograms
  list_h, __list_f = listh( hnm, list_filenm )
  print(list_h)
  # normalisat to 1
  norm2unity(list_h)
  # set styles
  hstyle(list_h, list_style)
  # rebin if needed

  islogy = False
  #islogy = True if 'HH_pT' in hnm else False # compare Powheg MG5_aMC@NLO only

  # make canvas with significance [choose isabove or below] !!!
  if 'mjj' in hnm: c, _aux_c = mkcanvas_significance( [list_h,list_title], isabove=True, legend_title=legend_title, legendx=0.55, atlas_label=atlas_label, islogy=islogy, draw_opt=list_draw_opt)
  if 'detajj' in hnm or 'dyjj' in hnm: c, _aux_c = mkcanvas_significance( [list_h,list_title], isabove=True, legend_title=legend_title, legendx=0.55, atlas_label=atlas_label, islogy=islogy, draw_opt=list_draw_opt)
  if 'xi' in hnm: c, _aux_c = mkcanvas_significance( [list_h,list_title], isabove=False, legend_title=legend_title, legendx=0.55, atlas_label=atlas_label, islogy=islogy, draw_opt=list_draw_opt)
  if 'ngapjet' in hnm: c, _aux_c = mkcanvas_significance( [list_h,list_title], isabove=False, legend_title=legend_title, legendx=0.55, atlas_label=atlas_label, islogy=islogy, draw_opt=list_draw_opt)

  # write to pdf
  if i == 0: c.Print( outpdf+'(' )
  if i > 0 and i < len(list_histogram_names) - 1: c.Print( outpdf )
  if i == len(list_histogram_names) - 1: c.Print( outpdf+')' )

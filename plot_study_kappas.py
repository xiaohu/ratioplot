import ROOT as R
from ratioplotlib.mkcanvas import *
from ratioplotlib.util import *

# this uses 0,1,20 to morph any kappa lambda shape to have a comparisom on mHH

### inputs ###
scheme='0,1,20' # actual value [basis lambda] increasing order of lambda !!!

#lambdavalue=[-20,-10,-5,-2,-1,0]
#outpdf = 'HH_lambdastudy1-PowhegFT-linearcomb-{0}.pdf'.format(scheme.replace(',','_'))

#lambdavalue=[5,20,15,10,1,2.4]
#outpdf = 'HH_lambdastudy2-PowhegFT-linearcomb-{0}.pdf'.format(scheme.replace(',','_'))

#lambdavalue=[4,3,5,6,7,8]
#outpdf = 'HH_lambdastudy3-PowhegFT-linearcomb-{0}.pdf'.format(scheme.replace(',','_'))

lambdavalue=[4,20,0,1,5,2.4]
outpdf = 'HH_lambdastudy4-PowhegFT-linearcomb-{0}.pdf'.format(scheme.replace(',','_'))

schemestr=['00','01','20'] # string in file names [basis lambda] increasing order of lambda !!!
legend_title  =  '#kappa_{#lambda} studies'
atlas_label = 'Simulation Internal' # 'Generator-level' # 'Simulation Internal' 
list_title    = [ '#kappa_{{#lambda}}={0}'.format(_kl) for _kl in lambdavalue ]
# only the basis samples here ...
list_filenm   = ['../LHE/HH_PWG_FT/FT-lambda{0}-hist.root'.format(_basisstr) for _basisstr in schemestr ] # [ target, [basis1,2,3] ]
list_style    = [ [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
                  [ 1, 2, R.kGreen, 0 ],
                  [ 1, 2, R.kBlack, 0 ],
                  [ 1, 2, R.kYellow, 0 ],
                  [ 1, 2, R.kGray, 0 ],
                  [ 1, 2, R.kRed, 0 ],
                ]
list_draw_opt = [ 'hist',
                  'hist',
                  'hist',
                  'hist',
                  'hist',
                  'hist' ]
### inputs ###

# get all histogram names
list_histogram_names = listhnm( list_filenm )
print(list_histogram_names)

# ATLAS style
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasStyle.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasLabels.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasUtils.C")
R.SetAtlasStyle()

for i,hnm in enumerate(list_histogram_names):
  print("Processing histogram {0}".format(hnm))

  list_h = []
  # get list of histograms [basis lambda] with actually length=3
  list_h_base, __list_f_base = listh( hnm, list_filenm )
  for _kl in lambdavalue:
    h_lc = lc_lambda( scheme, float(_kl), 1, list_h_base )
    list_h.append(h_lc)

  # normalisat to 1
  norm2unity(list_h)

  # set styles
  hstyle(list_h, list_style)
  # rebin if needed
  if 'H1_pT' in hnm or 'H1_E' in hnm : hrebin(list_h, 20)
  if 'H2_pT' in hnm or 'H2_E' in hnm : hrebin(list_h, 20)
  if '_eta' in hnm or '_phi' in hnm : hrebin( list_h, 4)
  if 'HH_pT' in hnm : hrebin(list_h, 10)
  if 'HH_E' in hnm : hrebin(list_h, 40)
  if 'HH_m' in hnm :
    _HH_m_binning = range(0,600, 20) + range(600,1040,40)
    list_h = hrebin(list_h, _HH_m_binning, [250,799])
  if 'HH_dEta' in hnm  or 'HH_dPhi' in hnm or 'HH_dR' in hnm : hrebin(list_h, 2)
  if 'gq_pT' in hnm : hrebin(list_h, 10)
  if 'gq_E' in hnm : hrebin(list_h, 20)
  if 'weight' in hnm :
    hrange(list_h, -0.5, 0.5 )
    for _i,_h in enumerate(list_h):
      print( 'Weight pos:neg [{0}] {1} {2} '.format( list_title[_i], _h.Integral(1,_h.FindBin(0)-1), _h.Integral(_h.FindBin(0),_h.GetNbinsX()) ) )
  islogy = True if 'HH_pT' in hnm else False # compare Powheg MG5_aMC@NLO only

  # make canvas with ratio
  c, _aux_c = mkcanvas( [list_h,list_title], legend_title=legend_title, legendx=0.55, islogy=islogy, atlas_label=atlas_label, draw_opt=list_draw_opt)
  # write to pdf
  if i == 0: c.Print( outpdf+'(' )
  if i > 0 and i < len(list_histogram_names) - 1: c.Print( outpdf )
  if i == len(list_histogram_names) - 1: c.Print( outpdf+')' )

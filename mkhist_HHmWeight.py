import ROOT as R
from ratioplotlib.util import *

# from 1 to 0
#nm_base = '../hh_mc_gghh_lhe/untar_cHHHp01d0-hist.root'
#nm_target = '../hh_mc_gghh_lhe/untar_cHHHp00d0-hist.root'
#nm_weight_h = 'weight-mHH-from-cHHHp01d0-to-cHHHp00d0'
#nm_weight = '{0}.root'.format(nm_weight_h)

nm_base = '../hh_mc_gghh_lhe/untar_cHHHp10d0-hist.root'
nm_target = '../hh_mc_gghh_lhe/untar_cHHHp20d0-hist.root'
nm_weight_h = 'weight-mHH-from-cHHHp10d0-to-cHHHp20d0'
nm_weight = '{0}.root'.format(nm_weight_h)
#
nm_hist = 'HH_m'

# file
f_base = R.TFile(nm_base)
f_target = R.TFile(nm_target)
f_weight = R.TFile(nm_weight,'RECREATE')

# hist
h_base = f_base.Get(nm_hist)
h_base.SetDirectory(0)

h_target = f_target.Get(nm_hist)
h_target.SetDirectory(0)

_list_h = [h_base, h_target]
hrebin( _list_h, 20, [200,1000] )
norm2unity(_list_h)

h_weight = h_target.Clone('reweight_on_mHH')
h_weight.Divide(h_base)

f_weight.Write()
f_weight.Save()
f_weight.Close()


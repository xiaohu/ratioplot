import ROOT as R
from ratioplotlib.mkcanvas import *
from ratioplotlib.util import *

### inputs ###
# +
year=2018
outpdf = 'Muon_extract_{0}.pdf'.format(year)
legend_title  =  'Year {0}'.format(year)
status_label = 'Preliminary'
list_title    = [ 'Data',
                  'DY aMC@NLO FXFX' ]
list_filenm   = [ '../muon-fastfeedbacktool/job/merged-singlemuon_{0}_fit.root'.format(year),
                  '../muon-fastfeedbacktool/job/merged-DYJetsToLL_M-50-{0}_fit.root'.format(year)]
list_style    = [ [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
                  [ 1, 2, R.kRed, 0 ] ]
list_draw_opt = [ 'hist',
                  'hist' ]
### inputs ###

# get all histogram names
list_histogram_names = listhnm( list_filenm )
print(list_histogram_names)

# ATLAS style
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasStyle.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasLabels.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasUtils.C")
R.SetAtlasStyle()

for i,hnm in enumerate(list_histogram_names):
  print("Processing histogram {0}".format(hnm))

  # get list of histograms
  list_h, __list_f = listh( hnm, list_filenm )
  print(list_h)
  # normalisat to 1
  #norm2unity(list_h)
  # set styles
  hstyle(list_h, list_style)
  # rebin if needed
  #
  # extract bin info
  # mean__1stpT__Muon_mediumId__Muon_pt
  _x = 0.22
  _y = 0.55
  _l = hnm.split('__')
  print(_l)
  _extract = _l[0]
  _ytitle = ''
  if _extract == 'mean':
    _extract = 'Energy scale'
    _y = 0.30
  if _extract == 'width':
    _extract = 'Energy resolution'
  _binmode = 'Mode {0}'.format(_l[1])
  _id = _l[2]
  _var = _l[3]
  _xtitle = ''
  if _var == 'Muon_pt':
    _xtitle = 'p_{T} [GeV]'
  if _var == 'Muon_eta':
    _xtitle = '#eta'
  _morelabel = [ [_x,_y, _extract], [_x,_y-0.05,_id], [_x,_y-0.10,_binmode] ]

  # ranges
  _yrange = []
  _ratiorange = []
  _ytitle
  if _extract == 'Energy scale':
    _yrange = [60,100]
    _ratiorange = [0.98,1.02]
    _ytitle = 'm(#mu#mu)'
  if _extract == 'Energy resolution':
    _yrange = [1,3]
    _ratiorange = [0.8,1.2]
    _ytitle = '#sigma(m(#mu#mu))'
  _ylegend = None
  if _extract == 'Energy scale':
    _ylegend = 0.55

  # make canvas with ratio
  c, _aux_c = mkcanvas( [list_h,list_title], yrange=_yrange, ratiorange=_ratiorange, xtitle=_xtitle, ytitle=_ytitle, legend_title=legend_title, legendx=_x, legendy=_ylegend, status_label=status_label, draw_opt=list_draw_opt, morelabel=_morelabel, autolegend=False)

  # write to pdf
  if i == 0: c.Print( outpdf+'(' )
  if i > 0 and i < len(list_histogram_names) - 1: c.Print( outpdf )
  if i == len(list_histogram_names) - 1: c.Print( outpdf+')' )

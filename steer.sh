## Ztautau kinematics
for chl in emu etau_1prong etau_3prong mutau_1prong mutau_3prong
do
python plot_ZttKine.py ${chl} Ztt_Kine_${chl}.pdf ../anatree/cut_z_mass_0/PowhegPythia8_Ztt_hist_${chl}.root ../anatree/cut_z_mass_0/Sherpa221_Ztt_hist_${chl}.root
python plot_ZttKine.py ${chl} Ztt_Kine_${chl}_mZ60.pdf ../anatree/cut_z_mass_60/PowhegPythia8_Ztt_hist_${chl}.root ../anatree/cut_z_mass_60/Sherpa221_Ztt_hist_${chl}.root
python plot_ZttKine.py ${chl} Ztt_Kine_${chl}_mZ60recoall.pdf ../anatree/cut_z_mass_60_recoall/PowhegPythia8_Ztt_hist_${chl}.root ../anatree/cut_z_mass_60_recoall/Sherpa221_Ztt_hist_${chl}.root

python plot_ZttKine.py ${chl} Ztt_Kine_${chl}_mZ60_sherpa2powheg.pdf ../anatree/cut_z_mass_60/PowhegPythia8_Ztt_hist_${chl}.root ../anatree/cut_z_mass_60_rwtSherpa2Powheg/Sherpa221_Ztt_hist_${chl}.root
done



## tt kinematics
##for chl in isWemu isWetauHad_1prong isWetauHad_3prong isWmutauHad_1prong isWmutauHad_3prong isWej isWmuj
#for chl in isWemu isWetauHad_1prong
#do
#python plot_ttKine.py ${chl} tt_Kine_${chl}.pdf ../anatree/tt_weight/tt_${chl}.root ../anatree/tt_weight_fsr_muR05/tt_${chl}.root ../anatree/tt_weight_fsr_muR0625/tt_${chl}.root ../anatree/tt_weight_fsr_muR0750/tt_${chl}.root ../anatree/tt_weight_fsr_muR0875/tt_${chl}.root ../anatree/tt_weight_fsr_muR1250/tt_${chl}.root ../anatree/tt_weight_fsr_muR1500/tt_${chl}.root ../anatree/tt_weight_fsr_muR1750/tt_${chl}.root ../anatree/tt_weight_fsr_muR20/tt_${chl}.root
#done



## RbbWW
#chl="RbbWW"
#python plot_ttKine.py ${chl} tt_Kine_${chl}.pdf ../anatree/tt_weight/tt_${chl}.root ../anatree/tt_weight_fsr_muR05/tt_${chl}.root ../anatree/tt_weight_fsr_muR20/tt_${chl}.root
